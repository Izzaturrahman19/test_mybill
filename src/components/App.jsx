import React from "react";

function App() {
    return (
        <div className="form">
            <div className="form-margin">
                <h3 className="white">Reg Card</h3>
                <form className="flex-form" action="https://www.mybillpayment.com/validation.php" method="post">
                    <input type="text" className="form-size" name="Command" placeholder="Command"/>
                    <input type="text" className="form-size" name="OrderNo" placeholder="OrderNo"/>
                    <input type="text" className="form-size" name="MemberId" placeholder="MemberId"/>
                    <input type="text" className="form-size" name="StatusUrl" placeholder="StatusUrl"/>
                    <input type="text" className="form-size" name="ReturnUrl" placeholder="ReturnUrl"/>
                    <input type="submit" className="form-size" value="Proceed"/>
                </form>
            </div>
            <div className="form-margin">
                <h3 className="white">Query Token</h3>
                <form className="flex-form" action="https://www.mybillpayment.com/validation.php" method="post">
                    <input type="text" className="form-size" name="Command" placeholder="Command"/>
                    <input type="text" className="form-size" name="CardToken" placeholder="CardToken"/>
                    <input type="text" className="form-size" name="StatusUrl" placeholder="StatusUrl"/>
                    <input type="submit" className="form-size" value="Proceed"/>
                </form>
            </div>
            <div className="form-margin">
                <h3 className="white">Authorization</h3>
                <form className="flex-form" action="https://www.mybillpayment.com/validation.php" method="post">
                    <input type="text" className="form-size" name="Command" placeholder="Command"/>
                    <input type="text" className="form-size" name="OrderNo" placeholder="OrderNo"/>
                    <input type="text" className="form-size" name="Amount" placeholder="Amount"/>
                    <input type="text" className="form-size" name="OrderDesc" placeholder="OrderDesc"/>
                    <input type="text" className="form-size" name="Name" placeholder="Name"/>
                    <input type="text" className="form-size" name="EmailAdd" placeholder="EmailAdd"/>
                    <input type="text" className="form-size" name="CardToken" placeholder="CardToken"/>
                    <input type="text" className="form-size" name="StatusUrl" placeholder="StatusUrl"/>
                    <input type="submit" className="form-size" value="Proceed"/>
                </form>
            </div>
            <div className="form-margin">
                <h3 className="white">Capture</h3>
                <form className="flex-form" action="https://www.mybillpayment.com/validation.php" method="post">
                    <input type="text" className="form-size" name="Command" placeholder="Command"/>
                    <input type="text" className="form-size" name="OrderNo" placeholder="OrderNo"/>
                    <input type="text" className="form-size" name="Amount" placeholder="Amount"/>
                    <input type="text" className="form-size" name="StatusUrl" placeholder="StatusUrl"/>
                    <input type="submit" className="form-size" value="Proceed"/>
                </form>
            </div>
            <div className="form-margin">
                <h3 className="white">Pay</h3>
                <form className="flex-form" action="https://www.mybillpayment.com/validation.php" method="post">
                    <input type="text" className="form-size" name="Command" placeholder="Command"/>
                    <input type="text" className="form-size" name="OrderNo" placeholder="OrderNo"/>
                    <input type="text" className="form-size" name="Amount" placeholder="Amount"/>
                    <input type="text" className="form-size" name="OrderDesc" placeholder="OrderDesc"/>
                    <input type="text" className="form-size" name="Name" placeholder="Name"/>
                    <input type="text" className="form-size" name="EmailAdd" placeholder="EmailAdd"/>
                    <input type="text" className="form-size" name="CardToken" placeholder="CardToken"/>
                    <input type="text" className="form-size" name="StatusUrl" placeholder="StatusUrl"/>
                    <input type="submit" className="form-size" value="Proceed"/>
                </form>
            </div>
            <div className="form-margin">
                <h3 className="white">Void Pay</h3>
                <form className="flex-form" action="https://www.mybillpayment.com/validation.php" method="post">
                    <input type="text" className="form-size" name="Command" placeholder="Command"/>
                    <input type="text" className="form-size" name="OrderNo" placeholder="OrderNo"/>
                    <input type="text" className="form-size" name="StatusUrl" placeholder="StatusUrl"/>
                    <input type="submit" className="form-size" value="Proceed"/>
                </form>
            </div>
        </div>
    );
}

export default App;